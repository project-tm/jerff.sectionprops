<?php

namespace Jerff\SectionProps\TraitList;

use Jerff\SectionProps\Entity;

trait Query {

    public static function queryCustom()
    {
        return new Entity\Query(static::getEntity());
    }

}