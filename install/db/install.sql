
CREATE TABLE IF NOT EXISTS `project_propsview_section` (
  `SECTION_ID` int(10) unsigned NOT NULL,
  `PROPS_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`SECTION_ID`,`PROPS_ID`),
  KEY `be_block_client_type_SECTION_ID_foreign` (`SECTION_ID`),
  KEY `be_block_client_type_PROPS_ID_foreign` (`PROPS_ID`)
/* CONSTRAINT `project_propsview_section_ibfk_SECTION_ID` FOREIGN KEY (`SECTION_ID`) REFERENCES `b_iblock_section` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,*/
 /* CONSTRAINT `project_propsview_section_ibfk_PROPS_ID` FOREIGN KEY (`PROPS_ID`) REFERENCES `b_iblock_property` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE*/
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `b_iblock_section_element` ADD KEY `project_propsview_IBLOCK_SECTION_ID` (`IBLOCK_SECTION_ID`);
ALTER TABLE `b_iblock_element_property` ADD KEY `project_propsview_IBLOCK_ELEMENT_ID_IBLOCK_PROPERTY_ID` (`IBLOCK_ELEMENT_ID`, `IBLOCK_PROPERTY_ID`);
ALTER TABLE `b_iblock_property` ADD KEY `project_propsview_IBLOCK_ID` (`IBLOCK_ID`);
