
DROP TABLE IF EXISTS project_propsview_section;

alter table `b_iblock_section_element` drop index `project_propsview_IBLOCK_SECTION_ID`;
alter table `b_iblock_element_property` drop index `project_propsview_IBLOCK_ELEMENT_ID_IBLOCK_PROPERTY_ID`;
alter table `b_iblock_property` drop index `project_propsview_IBLOCK_ID`;