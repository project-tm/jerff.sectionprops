<?php

if (!defined('\Project\Tools\Modules\IS_START')) {
    include_once(dirname(__DIR__) . '/project.tools/include.php');
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;
use Project\Tools\Modules;

Loc::loadMessages(__FILE__);

class jerff_sectionprops extends CModule
{
    public $MODULE_ID = 'jerff.sectionprops';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;

    use Modules\Install;

    function __construct()
    {
        $this->setParam(__DIR__, 'JERFF_SECTION_PROPS');
        $this->MODULE_NAME = Loc::getMessage('JERFF_SECTION_PROPS_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('JERFF_SECTION_PROPS_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('JERFF_SECTION_PROPS_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('JERFF_SECTION_PROPS_PARTNER_URI');
    }

    public function DoInstall()
    {
        $this->Install();
    }

    public function DoUninstall()
    {
        $this->Uninstall();
    }

    public function InstallDb()
    {
        global $DB;
        $this->UnInstallDb();
        $DB->RunSQLBatch(__DIR__ . '/db/install.sql');
    }

    public function UnInstallDb()
    {
        global $DB;
        $DB->RunSQLBatch(__DIR__ . '/db/uninstall.sql');
    }

    public function InstallAgent()
    {
        $name = 'Jerff\SectionProps\Agent\Section::update();';
        $res = CAgent::GetList([], ["NAME" => $name]);
        if (!$row = $res->Fetch()) {
            CAgent::AddAgent(
                $name,
                $this->MODULE_ID,
                'Y',
                60 * 60 * 24,
                (new DateTime(date("Y-m-d 02:00:00"), "Y-m-d H:i:s"))->toString(),
                'Y',
                (new DateTime(date("Y-m-d 02:00:00"), "Y-m-d H:i:s"))->add('1 day')->toString()
            );
        }
        return true;
    }

    public function UnInstallAgent()
    {
        CAgent::RemoveAgent('Jerff\SectionProps\Agent\Section::update();', $this->MODULE_ID);
        return true;
    }

}